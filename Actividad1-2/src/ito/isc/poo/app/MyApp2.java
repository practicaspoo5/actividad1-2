package ito.isc.poo.app;
import java.util.Scanner;

public class MyApp2 {
    
    public static void Inicializar(double Calificaciones[], String Alumnos[], Scanner Teclado){
        for (int i = 0; i < Calificaciones.length; i++){
            System.out.print("De el nombre del alumno ");
            Alumnos[i] = Teclado.next();
            System.out.print("Ingrese la calificacion ");
            Calificaciones[i] = Teclado.nextDouble();
        }
    }
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    public static double Promedio(double Calificaciones[])
    {
        double prom, suma = 0;
        for (int i = 0; i < Calificaciones.length; i++)
            suma += Calificaciones[i];
        prom = suma / Calificaciones.length;
        return prom;
    }
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    public static void Imprimir(double Calificaciones[], String Alumnos[], double Promedio){
        System.out.printf("%-30s %-30s%n%n", "Alumnos", "Calificacion");
        for (int i = 0; i < Calificaciones.length; i++){
            System.out.printf("%-30s %-30f%n", Alumnos[i], Calificaciones[i]);
        }
        System.out.println("\nEl promedio grupal es "+Promedio);
    }
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    public static void Porcentaje(double Calificaciones[])
    {
    	int acreditados,reprobados, acreditadosConDificultades;
        acreditados=0;
        reprobados=0;
        acreditadosConDificultades=0;
        
        for(int i=0; i<Calificaciones.length; i++) {
        	if(Calificaciones[i]<70) {
        		reprobados++;
        	}else {
        		if(Calificaciones[i]>=70 && Calificaciones[i]<80) {
        			acreditadosConDificultades++;
        		}else {
        			acreditados++;
        		}
        	}
        }
        
        
        
        System.out.println("ACREDITADOS: "+((acreditados*100)/Calificaciones.length)+"%");
        System.out.println("REPROBADOS: "+((reprobados*100)/Calificaciones.length)+"%");
        System.out.println("ACREDITADOSCONDIFICULTADES: "+((acreditadosConDificultades*100)/Calificaciones.length)+"%");
    }
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    public static void main(String[] args) {
        Scanner Teclado = new Scanner(System.in);
        
        double Calificaciones[];
        String Alumnos[];
        int n;
        double Promedio;
        
        System.out.print("Ingrese la cantidad de alumnos ");
        n = Teclado.nextInt();
        
        Calificaciones = new double[n];
        Alumnos = new String[n];
        
        Inicializar(Calificaciones, Alumnos, Teclado);
        Promedio = Promedio(Calificaciones);
        Imprimir(Calificaciones, Alumnos, Promedio);
        Porcentaje(Calificaciones); 
    }
}
